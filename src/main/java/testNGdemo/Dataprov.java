package testNGdemo;
import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import Excelclassdemo.Excelldatafetch;
public class Dataprov extends Annotations
{
	@BeforeTest
	public void setdata()
	{
		exelFileName="createleadexcel";
	}
	@Test(dataProvider="getdata")
	public void runcreate(String a,String b, String c)
	{
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys(a);
		driver.findElementById("createLeadForm_firstName").sendKeys(b);
		driver.findElementById("createLeadForm_lastName").sendKeys(c);
		driver.findElementByXPath("//input[@name='submitButton']").click();
	}
	
	
}
