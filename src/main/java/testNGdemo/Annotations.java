package testNGdemo;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import Excelclassdemo.Excelldatafetch;

public class Annotations 
{
	String exelFileName;
	public ChromeDriver driver;
	@Parameters({"username","password"})
	@BeforeMethod
	public void login(String name,String pass)
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys(name);
		driver.findElementById("password").sendKeys(pass);
		driver.findElementByClassName("decorativeSubmit").click();
	}
	@AfterMethod
	public void close()
	{
		driver.close();
	}
	@DataProvider
	public String[][] getdata() throws InvalidFormatException, IOException
	{
		
		return Excelldatafetch.readexcel(exelFileName);
	}
}
