package testNGdemo;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import net.bytebuddy.implementation.bytecode.ByteCodeAppender.Size;
public class CreateLead extends Annotations
{
	@Test
	public void runcreate() throws InterruptedException 
	{
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("cognizant");
		driver.findElementById("createLeadForm_firstName").sendKeys("lima");
		driver.findElementById("createLeadForm_lastName").sendKeys("priya");
		/*WebElement dropdown1 = driver.findElementById("createLeadForm_dataSourceId");
		Select dd=new Select(dropdown1);
		dd.selectByVisibleText("Existing Customer");
		WebElement dd2 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select d=new Select(dd2);
		List<WebElement> options = d.getOptions();
		int s=options.size();
		System.out.println(s);
		d.selectByIndex(options.size()-1);*/
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("nithu");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("vemula");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Dear");
		Select source=new Select(driver.findElementById("createLeadForm_dataSourceId"));
		source.selectByVisibleText("Employee");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Mr");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("100000");
		Select industry=new Select(driver.findElementById("createLeadForm_industryEnumId"));
		industry.selectByVisibleText("Computer Software");
		Select owner=new Select(driver.findElementById("createLeadForm_ownershipEnumId"));
		owner.selectByVisibleText("Corporation");
		driver.findElementById("createLeadForm_sicCode").sendKeys("1234568585");
		driver.findElementById("createLeadForm_description").sendKeys("im bad boy");
		driver.findElementById("createLeadForm_importantNote").sendKeys("dont trust humans");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("91");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("600");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("dont postpone");
		driver.findElementById("createLeadForm_departmentName").sendKeys("ECE");
		Select currency=new Select(driver.findElementById("createLeadForm_currencyUomId"));
		currency.selectByVisibleText("INR - Indian Rupee");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("23");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("bat");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("kohli");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys(driver.getCurrentUrl());//current url
		driver.findElementById("createLeadForm_generalToName").sendKeys("prabhash");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("puthalapattu");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("chittoor");
		driver.findElementById("createLeadForm_generalCity").sendKeys("chittoor");
		Select contry=new Select(driver.findElementById("createLeadForm_generalCountryGeoId"));
		contry.selectByVisibleText("India");
		Select state=new Select(driver.findElementById("createLeadForm_generalStateProvinceGeoId"));
		state.selectByVisibleText("Hawaii");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("517124");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("12345");
		Select mc=new Select(driver.findElementById("createLeadForm_marketingCampaignId"));
		mc.selectByVisibleText("Automobile");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9985591433");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("vemula4e3@gmail.com");
		driver.findElementByXPath("//input[@name='submitButton']").click();
	}
	
}
