package testNGdemo;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class DuplicateLead extends Annotations{
	@Test
	public void rundup() throws InterruptedException 
	{
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByLinkText("Email").click();
		driver.findElementByXPath("//input[@name='emailAddress']").sendKeys("vemula4e3@gmail.com");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(1000);
		WebElement fname = driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-firstName'])[1]/a");
		String fn=fname.getText();
		System.out.println("name of lead= "+fn);
		Thread.sleep(500);
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
		driver.findElementByXPath("//a[text()='Duplicate Lead']").click();
		System.out.println(driver.getTitle());
		Thread.sleep(1000);
		String fn2=driver.findElementById("createLeadForm_firstName").getAttribute("value");
		System.out.println(fn2);
		driver.findElementByXPath("//input[@name='submitButton']").click();
		if(fn.equals(fn2))
		{
			System.out.println("names are same");
		}
		else
			System.out.println("names are not same");
	}

}
