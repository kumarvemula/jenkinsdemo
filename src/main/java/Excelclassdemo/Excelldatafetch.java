package Excelclassdemo;
import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excelldatafetch {

	public static String[][] readexcel(String path) throws InvalidFormatException, IOException 
	{
		XSSFWorkbook workbook=new XSSFWorkbook(new File("./data/"+path+".xlsx"));
		XSSFSheet worksheet = workbook.getSheetAt(0);
		int rowcount = worksheet.getLastRowNum();
		System.out.println(rowcount);
		int cellcount = worksheet.getRow(0).getLastCellNum();
		System.out.println(cellcount);
		String[][] data=new String[rowcount][cellcount];
		for(int i=1;i<=rowcount;i++)
		{
			XSSFRow row = worksheet.getRow(i);
			for(int j=0;j<cellcount;j++)
			{
				XSSFCell cell = row.getCell(j);
				data[i-1][j]=cell.getStringCellValue();
				
			}
		}
		workbook.close();
		return data;
	}
}
