package Excelclassdemo;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import testNGdemo.Annotations;
public class EditLead extends Annotations
{
	@BeforeMethod
	 public void editexcel()
	{
		String s="edit.xlsx";
	}
	@Test
	public void runedit() throws InterruptedException
	{
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("LIMA");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		//driver.findElementByXPath("//a[text()='10237']").click();
		Thread.sleep(1000);
		WebDriverWait wait=new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"))).click();
		String title=driver.getTitle();
		Thread.sleep(500);
		System.out.println("title of the page= "+title);
		driver.findElementByXPath("//a[text()='Edit']").click();
		driver.findElementById("updateLeadForm_companyName").clear();
		Thread.sleep(1000);
		driver.findElementById("updateLeadForm_companyName").sendKeys("cts");
		driver.findElementByXPath("(//input[@name='submitButton'])[1]").click();
	}

}
