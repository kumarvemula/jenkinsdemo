package week4.day1;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
public class Windowclass {

	public static void main(String[] args) throws IOException 
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.irctc.co.in/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		File ss = driver.getScreenshotAs(OutputType.FILE);
		File obj=new File("./screenshots/image.png");
		FileUtils.copyFile(ss, obj);
		String cwin = driver.getWindowHandle();
		driver.findElementByLinkText("Contact Us").click();
		List<String> l1=new ArrayList<String>();
		Set<String> s1=driver.getWindowHandles();
		l1.addAll(s1);
		WebDriver window = driver.switchTo().window(l1.get(1));
		driver.switchTo().window(cwin).close();
	}
}
