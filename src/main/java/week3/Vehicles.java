package week3;

public interface Vehicles 
{
	public void accidentAlarm();
	
	public abstract void enableQuietMode();
	
	public void autoAirFill();
}
