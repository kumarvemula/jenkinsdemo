package com.testleaf;
import java.util.List;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import io.cucumber.datatable.dependency.com.fasterxml.jackson.databind.PropertyNamingStrategy.KebabCaseStrategy;
import net.bytebuddy.implementation.bytecode.ByteCodeAppender.Size;

public class Isrtc {

	public static void main(String[] args) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		driver.findElementByXPath("//input[@id='userRegistrationForm:userName']").sendKeys("nit3_6");
		driver.findElementById("userRegistrationForm:password").sendKeys("Nithin@4e3");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Nithin@4e3");
		WebElement sq = driver.findElementById("userRegistrationForm:securityQ");
		Select q1=new Select(sq);
		q1.selectByVisibleText("What is your pet name?");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("kittu");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("nithin");
		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		WebElement day = driver.findElementById("userRegistrationForm:dobDay");
		Select db1=new Select(day);
		db1.selectByVisibleText("13");
		WebElement month = driver.findElementById("userRegistrationForm:dobMonth");
		Select db2=new Select(month);
		db2.selectByVisibleText("FEB");
		WebElement year = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select db3=new Select(year);
		db3.selectByVisibleText("1996");
		WebElement occ = driver.findElementById("userRegistrationForm:occupation");
		Select job=new Select(occ);
		job.selectByVisibleText("Private");
		WebElement contry = driver.findElementById("userRegistrationForm:countries");
		Select in=new Select(contry);
		in.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:email").sendKeys("nithin.vemula2013@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9985591433");
		WebElement nation = driver.findElementById("userRegistrationForm:nationalityId");
		Select india=new Select(nation);
		india.selectByVisibleText("India");
		driver.findElementByXPath("//input[@id='userRegistrationForm:address']").sendKeys("5-13");
		driver.findElementByXPath("//input[@id='userRegistrationForm:pincode']").sendKeys("517124");
		WebElement tab=driver.findElementByXPath("//input[@id='userRegistrationForm:pincode']");
		tab.sendKeys(Keys.TAB);
		Thread.sleep(1000);
		WebElement dis = driver.findElementByXPath("//select[@id='userRegistrationForm:cityName']");
		Select s=new Select(dis);
		s.selectByValue("Chittoor");
		Thread.sleep(2000);
		WebElement post = driver.findElementByXPath("//select[@id='userRegistrationForm:postofficeName']");
		Select sp=new Select(post);
		sp.selectByValue("Thalupulapalle B.O");
		driver.findElementByXPath("//input[@id='userRegistrationForm:landline']").sendKeys("9985591433");
	}
}
