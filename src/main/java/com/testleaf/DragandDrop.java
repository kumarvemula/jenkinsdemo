package com.testleaf;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import io.cucumber.datatable.dependency.com.fasterxml.jackson.databind.PropertyNamingStrategy.KebabCaseStrategy;
import net.bytebuddy.implementation.bytecode.ByteCodeAppender.Size;

public class DragandDrop {

	public static void main(String[] args) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		//driver.get("http://jqueryui.com/draggable/");
		driver.get("http://jqueryui.com/sortable/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.switchTo().frame(0);
		/*WebElement e1 = driver.findElementById("draggable");
		Actions builder=new Actions(driver);
		builder.clickAndHold(e1).moveToElement(e1, 150, 200).release().perform();
		driver.findElementByLinkText("Sortable").click();*/
		
		WebElement s2 = driver.findElementByXPath("(//ul[@id='sortable']/li)[1]");
		WebElement d2 = driver.findElementByXPath("(//ul[@id='sortable']/li)[4]");
		int x= s2.getLocation().getX();
		int y=d2.getLocation().getY();
		Actions builder=new Actions(driver);
		builder.dragAndDropBy(s2, x, y).perform();
			
	}
}
