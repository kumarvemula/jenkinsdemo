package com.testleaf;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DeleteLead {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemosalesManager");;
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByLinkText("Phone").click();
		driver.findElementByXPath("//input[@name='phoneCountryCode']").sendKeys("91");
		Thread.sleep(1000);
		WebElement tab1=driver.findElementByXPath("//input[@name='phoneCountryCode']");
		tab1.sendKeys(Keys.TAB);
		Thread.sleep(500);
		driver.findElementByXPath("//input[@name='phoneAreaCode']").sendKeys("600");
		WebElement tab2=driver.findElementByXPath("//input[@name='phoneNumber']");
		tab2.sendKeys(Keys.TAB);
		driver.findElementByXPath("//input[@name='phoneNumber']").sendKeys("9985591433");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(1000);
		WebElement leadid=driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		String li=leadid.getText();
		System.out.println("lead id= "+li);
		Thread.sleep(500);
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
		driver.findElementByXPath("//a[text()='Delete']").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//input[@name='id']").sendKeys(li);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		String error=driver.findElementByXPath("//div[text()='No records to display']").getText();
		System.out.println(error);
		
		
	}

}
