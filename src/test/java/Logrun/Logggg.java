package Logrun;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Logggg 
{
	public ChromeDriver driver;
	
	@Given("open chrome browser")
	public void openChromeBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 driver=new ChromeDriver();
	}

	@Given("Maximize the browser")
	public void maximizeTheBrowser() {
		
		driver.manage().window().maximize();
	}

	@Given("Load the URL")
	public void loadTheURL() {
		driver.get("http://leaftaps.com/opentaps");
	}

	@Given("enter username")
	public void enterUsername() {
		driver.findElementById("username").sendKeys("DemosalesManager");
	}

	@Given("enter password")
	public void enterPassword() {
		driver.findElementById("password").sendKeys("crmsfa");
	}

	@When("click the login button")
	public void clickTheLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	

	@Then("user clicks on CRM\\/SFA link")
	public void userClicksOnCRMSFALink() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@When("user clicks on create lead")
	public void userClicksOnCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@When("enters the firstname")
	public void entersTheFirstname() {
		driver.findElementById("createLeadForm_firstName").sendKeys("nithin");
	}

	@When("enters the last name")
	public void entersTheLastName() {
		driver.findElementById("createLeadForm_lastName").sendKeys("vemula");
	}

	@When("clicks on submit button")
	public void clicksOnSubmitButton() {
		driver.findElementByXPath("//input[@name='submitButton']").click();
	}

	@But("empty last name")
	public void emptylast()
	{
		System.out.println("last name is empty");
	}
	@And("enters company name")
	public void enterscompanyname()
	{
		driver.findElementById("createLeadForm_companyName").sendKeys("cognizant");
	}
	@Then("close the window")
	public void close()
	{
		driver.close();
	}
}
